from random import randint, sample
import os
from datetime import datetime


template_question_start = f"""\\documentclass[12pt,a4paper]{{scrartcl}}
\\pagestyle{{empty}}
\\usepackage[left=1.0cm,right=1.5cm,top=1.0cm,bottom=1.0cm]{{geometry}}
\\usepackage[utf8]{{inputenc}}
\\usepackage[english,russian]{{babel}}
\\usepackage{{misccorr}}
\\begin{{document}}
\\textbf{{\\large Контрольная работа: "Целые числа в памяти компьютера"}}\\\\"""

template_question_end = f"""
\\end{{document}}"""



template_answer_start = f"""
\\documentclass[12pt,a4paper]{{scrartcl}}
\\pagestyle{{empty}}
\\usepackage[left=1.0cm,right=1.5cm,top=1.0cm,bottom=1.0cm]{{geometry}}
\\usepackage[utf8]{{inputenc}}
\\usepackage[english,russian]{{babel}}
\\usepackage{{misccorr}}
\\usepackage{{multicol}}
\\begin{{document}}
\\ttfamily
\\begin{{multicols}}{{3}}"""

template_answer_end = f"""
\\end{{multicols}}
\\end{{document}}
"""

format = "%Y%m%d_%H%M%S"

filename_question = f"question_{str(datetime.now().strftime(format))}.tex"
filename_answer = f"answer_{str(datetime.now().strftime(format))}.tex"

fquestion = open(filename_question, mode="w", encoding="utf-8")
fanswer = open(filename_answer, mode="w", encoding="utf-8")

fquestion.write(template_question_start)
fanswer.write(template_answer_start)

variants = 30

for var in range(1, variants+1):
    question_1 = randint(50, 125)

    question_2 = randint(50, 126)

    question_3_1 = randint(150, 240)
    question_3_2 = 256 - question_3_1 + randint(5, 20)

    question_4_1 = randint(80, 110)
    question_4_2 = 127 - question_4_1 + randint(5, 20)

    question_5_1 = randint(30000, 35000)
    question_5_2 = 2**16 - question_5_1 + randint(20, 99)

    question_6_1 = randint(12000, 22000)
    question_6_2 = 32767 - question_6_1 + randint(20, 99)

    question_7_1 = sorted(sample(range(0, 16), k=5))
    question_7_2 = ["0"] * 16
    for i in question_7_1:
        question_7_2[i] = "1"

    question_8_1 = sorted(sample(range(0, 16), k=5))
    question_8_2 = ["1"] * 16
    for i in question_8_1:
        question_8_2[i] = "0"
        
    question_9_1 = randint(16, 255)
    question_9_2 = randint(16, 255)
    question_9_1_txt = "{" + hex(question_9_1)[2:].upper() + "_{16}}"
    question_9_2_txt = "{" + hex(question_9_2)[2:].upper() + "_{16}}"

    question_10_1 = randint(3, 7)
    question_10_2 = randint(10, 15)

    question_10_3 = ["0"] * 16
    for i in range(question_10_1, question_10_2 + 1):
        question_10_3[15 - i] = "1"

    question_10_4 = ["0"] * 16
    for i in range(question_10_2 - question_10_1 + 1):
        question_10_4[15 - i] = "1"
        

    template_question_tmp = f"""
    \\textbf{{Вариант {var}}}
    \\begin{{enumerate}}
    \\item 
    Запишите число {question_1} в 8-битную ячейку памяти.

    \\item
    Запишите число -{question_2} в 8-битную ячейку памяти.

    \\item
    Какое число получится, если сложить {question_3_1} и {question_3_2}, в 8-битной арифметике без знака?

    \\item
    Какое число получится если сложить {question_4_1} и {question_4_2}, в 8-битной арифметике со знаком?

    \\item
    Какое число получится если сложить {question_5_1} и {question_5_2}, в 16-битной арифметике без знака?

    \\item
    Какое число получится если сложить {question_6_1} и {question_6_2}, в 16-битной арифметике со знаком?

    \\item
    Напишите логическую операцию и маску (в шестнадцатеричной системе счисления), которые нужно применить, чтобы ОБНУЛИТЬ биты {question_7_1} 16-битного целого числа (биты нумеруются справа налево, начиная с 0).

    \\item
    Напишите логическую операцию и маску (в шестнадцатеричной системе счисления), которые нужно применить, чтобы УСТАНОВИТЬ биты {question_8_1} 16-битного целого числа.

    \\item
    Выполните логическую операцию «исключающее ИЛИ» между числами $\\mathrm{question_9_1_txt}$ и $\\mathrm{question_9_2_txt}$ и запишите в ответе полученное число в шестнадцатеричной системе счисления.

    \\item
    Число записано в битах c {question_10_1} по {question_10_2} 16-битного целого числа, хранящегося в переменной N. Какие операции нужно выполнить, чтобы записать это число в переменную X? Маски для выполнения логических операций записывайте в шестнадцатеричной системе счисления.

    \\end{{enumerate}}
    """

    template_answer_tmp = f"""Вариант {var} \\\\
    1. {question_1:08b} \\\\
    2. {(question_2 - 1)^0b11111111:08b}  \\\\
    3. {(question_3_1 + question_3_2)%256} \\\\
    4. -{255 - (question_4_1 + question_4_2)} \\\\
    5. {(question_5_1 + question_5_2)%65536} \\\\
    6. -{(65535 - (question_6_1 + question_6_2))} \\\\
    7. {hex(int("".join(question_7_2), 2))} \\\\
    8. {hex(int("".join(question_8_2), 2))} \\\\
    9. {hex(question_9_1^question_9_2)} \\\\
    10. (N \& {hex(int("".join(question_10_3), 2))}) >> {question_10_1} \\\\
        (N >> {question_10_1}) \& {hex(int("".join(question_10_4), 2))}
    
    """
    fquestion.write(template_question_tmp)
    if var % 2 == 0:
        fquestion.write("\\newpage")
    fanswer.write(template_answer_tmp)

fquestion.write(template_question_end)
fanswer.write(template_answer_end)

fquestion.close()
fanswer.close()

os.system(f"pdflatex {filename_question}")
os.system(f"pdflatex {filename_answer}")

os.remove(f"{filename_question[:-4]}.aux")
os.remove(f"{filename_answer[:-4]}.aux")

os.remove(f"{filename_question[:-4]}.log")
os.remove(f"{filename_answer[:-4]}.log")

os.remove(f"{filename_question}")
os.remove(f"{filename_answer}")